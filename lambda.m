function lambda = lambda(matrix)
f = critCircuit(matrix);
lambda = f(1,2);
end